# Music Store

Views tracks and songs in html using thymeleaf.
API for handling customers.

## Demonstrates usage of thymeleaf:

"/home" shows 5 random artists, genres and tracks and a search bar.

"/search" displays search results for albums, artists, genres, and tracks. If the search parameter is empty it redirects.

"/" redirects to home

## API endpoints:
For full details see  [postman documentation link](https://documenter.getpostman.com/view/12300473/TVKJyaVg) or postman folder inside project.
The following end points exists:

### CRU: methods for customers:

GET /api/v1/customers

PUT /api/v1/customers

POST /api/v1/customers

### Plus the following end points also exists:
GET /api/v1/customers/:customerId/popular/genre

GET /api/v1/customers/countries

GET /api/v1/customers/spending/highest

# Demo of application on heroku using Docker:

https://experis-academy-musicstore.herokuapp.com

# Documentation of API endpoints:

https://documenter.getpostman.com/view/12300473/TVKJyaVg

# Docker image:

https://hub.docker.com/r/anetteos/musicstore
