package com.experis.musicStore.data_access;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CustomerRepositoryTest {
    CustomerRepository customerRepository = new CustomerRepository();

    @Test
    public void getCustomers() {
        assertEquals(3, customerRepository.getCustomers(3,0).size());
        assertEquals(1, customerRepository.getCustomers(1, 0).get(0).getCustomerId());
        assertEquals(2, customerRepository.getCustomers(1, 1).get(0).getCustomerId());

    }

}
