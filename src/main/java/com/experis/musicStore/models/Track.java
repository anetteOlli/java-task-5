package com.experis.musicStore.models;

public class Track {
    private int trackId;
    private String name;
    private String albumTitle;
    private String artistName;
    private float unitPrice;
    private String genreName;





    public Track() {}

    public Track(int trackId, String name,
                 float unitPrice, String albumTitle,
                 String artistName, String genreName) {
        this.trackId = trackId;
        this.name = name;
        this.unitPrice = unitPrice;
        this.albumTitle = albumTitle;
        this.artistName = artistName;
        this.genreName = genreName;
    }

    // Setters and getters


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }
    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}
