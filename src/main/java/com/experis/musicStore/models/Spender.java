package com.experis.musicStore.models;

public class Spender {
    private int customerId;
    private int amountBilled;
    private String firstName;
    private String lastName;

    public Spender(int customerId, int amountBilled, String firstName, String lastName) {
        this.customerId = customerId;
        this.amountBilled = amountBilled;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    // Setters and getters:


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getAmountBilled() {
        return amountBilled;
    }

    public void setAmountBilled(int amountBilled) {
        this.amountBilled = amountBilled;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
