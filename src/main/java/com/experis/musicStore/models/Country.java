package com.experis.musicStore.models;

public class Country {
    private String name;
    private int numberOfCustomers;

    public Country(String name, int numberOfCustomers) {
        this.name = name;
        this.numberOfCustomers = numberOfCustomers;
    }

    // Setters and getters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }
}
