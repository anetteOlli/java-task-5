package com.experis.musicStore.controllers;

import com.experis.musicStore.data_access.CountriesRepository;
import com.experis.musicStore.data_access.CustomerRepository;
import com.experis.musicStore.data_access.GenreRepository;
import com.experis.musicStore.data_access.SpenderRepository;
import com.experis.musicStore.models.Country;
import com.experis.musicStore.models.Customer;
import com.experis.musicStore.models.Spender;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    CustomerRepository customerRepository = new CustomerRepository();
    CountriesRepository countriesRepository = new CountriesRepository();
    SpenderRepository spenderRepository = new SpenderRepository();
    GenreRepository genreRepository = new GenreRepository();

    /**
     *
     * @param limit optional parameter - maximum numbers of customers returned. If not set, default is 20.
     * @param offset optional parameter - how many customers should be skipped before collecting
     * @return a list of customers
     */
    @GetMapping(value = "/customers")
    public ArrayList<Customer> getGenresInTheWrongAPI(@RequestParam(value = "limit", required = false, defaultValue = "20") String limit,
                                                      @RequestParam(value = "offset", required = false, defaultValue = "0") String offset){
        int limitNumber  = 20;
        int offsetNumber = 0;
        try {
            limitNumber = Integer.parseInt(limit);
            offsetNumber = Integer.parseInt(offset);
        }catch (NumberFormatException e) {
            System.out.println(e.toString());
        }
        return customerRepository.getCustomers(limitNumber, offsetNumber);
    }

    /**
     *
     * @param customer with or withouth customerId field filled out. Will overwrite customerID if it is filled out.
     * @return 201 created with customer object - if everything went successfully.
     * @return 400 with description of what attributes are missing
     * @return 500 - if database problems occurs.
     */
    @PostMapping(value = "/customers")
    public ResponseEntity<?> postCustomer(@RequestBody Customer customer) {
        if(customer.getFirstName() == null) {
            return new ResponseEntity<>("missing firstName", HttpStatus.BAD_REQUEST);
        } else if (customer.getLastName() == null) {
            return new ResponseEntity<>("missing lastName", HttpStatus.BAD_REQUEST);
        }else if (customer.getCountry() == null) {
            return new ResponseEntity<>("missing country", HttpStatus.BAD_REQUEST);
        } else if (customer.getPhoneNumber() == 0) {
            return new ResponseEntity<>("invalid phoneNumber", HttpStatus.BAD_REQUEST);
        } else if (customer.getPostalCode() == 0) {
            return new ResponseEntity<>("invalid postalCode", HttpStatus.BAD_REQUEST);
        }else if(customer.getEmail() == null) {
            return new ResponseEntity<>("missing email", HttpStatus.BAD_REQUEST);
        }
        boolean createdCustomer = customerRepository.addCustomer(customer);
        if (!createdCustomer) {
            return new ResponseEntity<>("Try again later", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(customer);
    }

    /**
     *
     * @param customer to be updated
     * @return status 200 with updated customer object - if everything went successfully
     * @return status 400 with description of what input was incorrect
     * @return status 500 if a database error occured.
     */
    @PutMapping(value = "/customers")
    public ResponseEntity<?> putCustomer(@RequestBody Customer customer) {
        if(customer.getFirstName() == null) {
            return new ResponseEntity<>("missing firstname", HttpStatus.BAD_REQUEST);
        } else if (customer.getLastName() == null) {
            return new ResponseEntity<>("missing lastname", HttpStatus.BAD_REQUEST);
        }else if (customer.getCountry() == null) {
            return new ResponseEntity<>("missing country", HttpStatus.BAD_REQUEST);
        } else if (customer.getPhoneNumber() == 0) {
            return new ResponseEntity<>("invalid phone number", HttpStatus.BAD_REQUEST);
        } else if (customer.getPostalCode() == 0) {
            return new ResponseEntity<>("invalid postal code", HttpStatus.BAD_REQUEST);
        } else if (customer.getCustomerId() == 0 ) {
            return new ResponseEntity<>("invalid customer id", HttpStatus.BAD_REQUEST);
        }else if(customer.getEmail() == null) {
            return new ResponseEntity<>("missing phoneNumber", HttpStatus.BAD_REQUEST);
        }

        boolean updatedCustomer = customerRepository.updateCustomer(customer);
        if ( !updatedCustomer) {
            return new ResponseEntity<>("internal server error", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.status(HttpStatus.OK).body(customer);
    }

    /**
     *
     * @return a list of countires with the number of customers. Sorted after number of customers, country with the most customers first.
     */
    @GetMapping(value = "/customers/countries")
    public ArrayList<Country> getContries(){
        return countriesRepository.getCountries();
    }

    /**
     *
     * @return the custmores that spends the most ordered by highest pender first
     */
    @GetMapping(value = "customers/spending/highest")
    public ArrayList<Spender> getSpenders() {
        return spenderRepository.getSpenders();
    }


    /**
     *
     * @param id of customer
     * @return top favorite genre for the supplied customer. If tie returns all the top genres.
     */
    @GetMapping(value = "customers/{id}/popular/genre")
    public ResponseEntity<?> getPopularGenres(@PathVariable String id){
        int idInt = 0;
        try {
            idInt = Integer.parseInt(id);
        }catch (NumberFormatException e) {
            return new ResponseEntity<>("invalid customer id", HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.status(HttpStatus.OK).body(genreRepository.getTheMostPopularGenre(idInt));

    }
}
