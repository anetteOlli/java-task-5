package com.experis.musicStore.controllers;

//imports
import com.experis.musicStore.data_access.AlbumRepository;
import com.experis.musicStore.data_access.TrackRepository;
import com.experis.musicStore.data_access.GenreRepository;
import com.experis.musicStore.data_access.ArtistRepository;
import com.experis.musicStore.models.Album;
import com.experis.musicStore.models.Artist;
import com.experis.musicStore.models.Genre;
import com.experis.musicStore.models.Track;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;


@Controller
public class HomeController {
    private ArtistRepository artistRepository = new ArtistRepository();
    private GenreRepository genreRepository = new GenreRepository();
    private TrackRepository trackRepository = new TrackRepository();
    private AlbumRepository albumRepository = new AlbumRepository();


    /**
     * Redirects users to home
     * @return redirects to /home
     */
    @GetMapping("/")
    public String home() {
        return "redirect:/home";
    }

    /**
     * Generates 5 random artists, genres, and tracks.
     * @param model used for thymleaf
     * @return home.html
     */
    @GetMapping("/home")
    public String home( Model model ) {
        model.addAttribute("artists", artistRepository.getFiveRandomArtists());
        model.addAttribute("genres", genreRepository.getFiveRandomGenres());
        model.addAttribute("tracks", trackRepository.getFiveRandomTracks());
        return "home";
    }

    /**
     *
     * @param term queried. will be used for quering albums titles, artists names, genres, and track names for partial matches.
     * @param model used for generating thymleaf html
     * @return search.html
     */
    @GetMapping("/search")
    public String search(@RequestParam(value = "search", required = false) String term, Model model ) {
        if ( term == null) {
            return "redirect:/home";
        }
        if (term.length() < 3) {
            return "redirect:/home";
        }
        ArrayList<Album> albums = albumRepository.getAlbumsContainingTitle(term);
        ArrayList<Artist> artists = artistRepository.getArtistContainingName(term);
        ArrayList<Genre> genres = genreRepository.getGenresContainingName(term);
        ArrayList<Track> tracks = trackRepository.getTracksContainingName(term);
        boolean foundNothing = (albums.size() == 0) || (artists.size() == 0) || (genres.size() == 0) || (tracks.size() == 0);
        model.addAttribute("term", term);
        model.addAttribute("albums", albums);
        model.addAttribute("artists", artists);
        model.addAttribute("genres" , genres);
        model.addAttribute("tracks", tracks);
        model.addAttribute("foundNothing", foundNothing);
        return "search";
    }
}
