package com.experis.musicStore.data_access;

import com.experis.musicStore.models.Album;
import java.sql.*;
import java.util.ArrayList;

public class AlbumRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn;
    private PreparedStatement prep;
    private ResultSet set;


    /**
     * Queries the database for albums that partially matches the term
     * @param title Does not require to be the complete title.
     * @return list of albums that matches the title supplied.
     */
    public ArrayList<Album> getAlbumsContainingTitle(String title) {
        title = title.toLowerCase();
        ArrayList<Album> albumWithArtists = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("SELECT AlbumId, Title, Name, Album.ArtistId as ArtistId FROM Album INNER JOIN Artist A on A.ArtistId = Album.ArtistId WHERE lower(Title) LIKE ?");
            prep.setString(1, "%" + title + "%");
            set = prep.executeQuery();
            while(set.next()) {
                albumWithArtists.add( new Album(
                        set.getInt("AlbumId"),
                        set.getString("Title"),
                        set.getInt("ArtistId"),
                        set.getString("Name")
                ));
            }
        }catch (SQLException e) {
            System.out.println(e.toString());
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }

        return albumWithArtists;
    }

}
