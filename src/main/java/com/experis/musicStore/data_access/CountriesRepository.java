package com.experis.musicStore.data_access;

import com.experis.musicStore.models.Country;
import java.sql.*;
import java.util.ArrayList;

public class CountriesRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn;
    private PreparedStatement prep;
    private ResultSet set;


    /**
     *
     * @return a list of countries and number of customers in each country sorted after the amount of customers.
     */
    public ArrayList<Country> getCountries(){
        ArrayList<Country> countries = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("SELECT Country, COUNT(CustomerId) as NumberOfCustomers FROM Customer GROUP BY Country ORDER BY NumberOfCustomers DESC");
            set = prep.executeQuery();
            while (set.next()) {
                countries.add( new Country(
                        set.getString("Country"),
                        set.getInt("NumberOfCustomers")
                ));
            }
        }catch (SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
        return countries;
    }
}
