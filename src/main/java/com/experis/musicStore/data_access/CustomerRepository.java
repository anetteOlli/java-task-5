package com.experis.musicStore.data_access;

import com.experis.musicStore.models.Customer;

import java.sql.*;
import java.util.ArrayList;

public class CustomerRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn;
    private PreparedStatement prep;
    private ResultSet set;

    /**
     *
     * @param numberOfCustomers size of list of customers returned
     * @param offset of customer starting point
     * @return list of customers with offset and size as specified.
     */
    public ArrayList<Customer> getCustomers(int numberOfCustomers, int offset) {
        String sqlWithoutOffsetAndLimit = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone FROM Customer";
        ArrayList<Customer> customers = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer LIMIT ?, ?");
            prep.setInt(1,offset);
            prep.setInt(2, numberOfCustomers);
            set = prep.executeQuery();
            while (set.next()) {
                customers.add( new Customer(
                        set.getInt("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getInt("PostalCode"),
                        set.getInt("Phone"),
                        set.getString("Email")
                ));
            }
        }catch (SQLException e) {
            System.out.println(e.toString());
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
        return customers;
    }


    /**
     * This function mutates the customer object
     * @param customer that will be stored in the database. Disregards the customerID and asigns a unique customer id.
     * @return true if it successfully created a customer.
     */
    public boolean addCustomer(Customer customer) {
        boolean result = false;

        try {
            conn = DriverManager.getConnection(URL);
            conn.setAutoCommit(false);
            prep = conn.prepareStatement("SELECT MAX(CustomerId) as customerID FROM Customer");
            set = prep.executeQuery();
            int vacantCustomerId = 0;
            if(set.next()){
                vacantCustomerId = set.getInt("customerId") + 1;

            }
            if(vacantCustomerId != 0) {
                customer.setCustomerId(vacantCustomerId);
                set.close();
                prep.close();
                prep = conn.prepareStatement("INSERT INTO Customer (CustomerId, FirstName, LastName, Country, PostalCode, Phone, SupportRepId, Email) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
                prep.setInt(1, vacantCustomerId);
                prep.setString(2, customer.getFirstName());
                prep.setString(3, customer.getLastName());
                prep.setString(4, customer.getCountry());
                prep.setInt(5, customer.getPostalCode());
                prep.setInt(6, customer.getPhoneNumber());
                prep.setInt(7, 1);
                prep.setString(8,customer.getEmail());
                int numberOfRows = prep.executeUpdate();
                if (numberOfRows == 1) {
                    result = true;
                }
                conn.commit();
                conn.setAutoCommit(true);
            }

        }catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
                return false;
            }
        }

        return result;
    }

    /**
     *
     * @param customer that will be updated.
     * @return true if it successfully updated the customer.
     * @return false if the customerId is invalid
     * @return false if and SQLException occured
     */
    public boolean updateCustomer(Customer customer) {
        boolean result = false;
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("UPDATE Customer SET FirstName = ?, LastName = ?, Country = ?, " +
                    "PostalCode= ?, Phone = ?, Email = ? WHERE CustomerId = ?");
            prep.setString(1, customer.getFirstName());
            prep.setString(2, customer.getLastName());
            prep.setString(3, customer.getCountry());
            prep.setInt(4, customer.getPostalCode());
            prep.setInt(5, customer.getPhoneNumber());
            prep.setString(6,customer.getEmail());
            prep.setInt(7, customer.getCustomerId());
            int numberOfRows = prep.executeUpdate();
            if (numberOfRows == 1) {
                result = true;
            }
        }catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        }finally {
            try {
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
                return false;
            }
        }

        return result;
    }

}

