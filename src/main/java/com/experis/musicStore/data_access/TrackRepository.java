package com.experis.musicStore.data_access;

import com.experis.musicStore.models.Track;

import java.sql.*;
import java.util.ArrayList;

public class TrackRepository {
    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn;
    PreparedStatement prep;
    ResultSet set;

    /**
     *
     * @return five random tracks
     */
    public ArrayList<Track> getFiveRandomTracks() {
        ArrayList<Track> tracks = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("SELECT TrackId, Track.Name as TrackName, UnitPrice, artist.Name as ArtistName, album.Title as AlbumTitle, genre.Name as GenreName " +
                    "FROM Track " +
                    "INNER JOIN Album album on album.AlbumId = Track.AlbumId " +
                    "INNER JOIN Artist artist on artist.ArtistId = album.ArtistId " +
                    "INNER JOIN Genre genre on Track.GenreId = genre.GenreId" +
                    " ORDER BY RANDOM() LIMIT 5");
            set = prep.executeQuery();
            while (set.next()) {
                tracks.add(new Track(
                        set.getInt("trackid"),
                        set.getString("TrackName"),
                        set.getFloat("unitPrice"),
                        set.getString("albumTitle"),
                        set.getString("ArtistName"),
                        set.getString("GenreName")
                        ));
            }
        }catch (SQLException e) {
            System.out.println(e.toString());
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }

        return tracks;
    }

    /**
     *
     * @param name of the track. Can be partially match
     * @return a list of tracks with partial match of the specified name.
     */
    public ArrayList<Track> getTracksContainingName(String name) {
        ArrayList<Track> tracks = new ArrayList<>();
        name = name.toLowerCase();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("SELECT TrackId, Track.Name as TrackName, UnitPrice, artist.Name as ArtistName, album.Title as AlbumTitle, genre.Name as GenreName " +
                    "FROM Track " +
                    "INNER JOIN Album album on album.AlbumId = Track.AlbumId " +
                    "INNER JOIN Artist artist on artist.ArtistId = album.ArtistId " +
                    "INNER JOIN Genre genre on genre.GenreId = Track.GenreId " +
                    "WHERE lower(track.Name) LIKE ?");
            prep.setString(1, "%"+ name + "%");
            set = prep.executeQuery();
            while (set.next()) {
                tracks.add( new Track(
                        set.getInt("TrackId"),
                        set.getString("TrackName"),
                        set.getFloat("Unitprice"),
                        set.getString("albumTitle"),
                        set.getString("ArtistName"),
                        set.getString("GenreName")
                ));
            }
        }catch (SQLException e) {
            System.out.println(e.toString());
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
        return  tracks;
    }
}
