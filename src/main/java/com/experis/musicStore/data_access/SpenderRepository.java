package com.experis.musicStore.data_access;

import com.experis.musicStore.models.Spender;

import java.sql.*;
import java.util.ArrayList;

public class SpenderRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn;
    private PreparedStatement prep;
    private ResultSet set;

    /**
     *
     * @return a list of customers that has spent money, ordered by the highest spenders.
     */
    public ArrayList<Spender> getSpenders(){
        ArrayList<Spender> spenders = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement(
                    "SELECT customer.CustomerId, customer.FirstName, customer.LastName, SUM(invoice.Total) as AmountBilled " +
                    "FROM Customer customer INNER JOIN Invoice invoice on customer.CustomerId = invoice.CustomerId " +
                    "GROUP BY customer.CustomerId, customer.FirstName, customer.LastName ORDER BY AmountBilled DESC");
            set = prep.executeQuery();
            while(set.next()) {
                spenders.add( new Spender(
                        set.getInt("CustomerId"),
                        set.getInt("AmountBilled"),
                        set.getString("FirstName"),
                        set.getString("LastName")
                ));
            }
        }catch (SQLException e){
            System.out.println(e.toString());
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }

        return spenders;
    }
}
