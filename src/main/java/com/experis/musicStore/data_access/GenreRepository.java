package com.experis.musicStore.data_access;

import com.experis.musicStore.models.Genre;
import java.sql.*;
import java.util.ArrayList;


public class GenreRepository {
    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn;
    PreparedStatement prep;
    ResultSet set;

    /**
     *
     * @return five random genres
     */
    public ArrayList<Genre> getFiveRandomGenres() {
        ArrayList<Genre> genres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("SELECT GenreId, Name FROM Genre ORDER BY RANDOM() LIMIT 5");
            set = prep.executeQuery();
            while (set.next()) {
                genres.add( new Genre(
                        set.getInt("GenreId"),
                        set.getString("Name")
                ));
            }
        }catch (SQLException e) {
            System.out.println(e.toString());
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }

        return genres;
    }


    /**
     *
     * @param name of the genre. can be partially match the name
     * @return a list of genres that matches the name partially.
     */
    public ArrayList<Genre> getGenresContainingName(String name) {
        name = name.toLowerCase();
        ArrayList<Genre> genres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("SELECT GenreId, Name From Genre WHERE lower(Name) LIKE ?");
            prep.setString(1, "%" + name + "%");
            set  = prep.executeQuery();
            while (set.next()) {
                genres.add( new Genre(
                        set.getInt("GenreId"),
                        set.getString("Name")
                ));
            }
        }catch (SQLException e) {
            System.out.println(e.toString());
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
        return genres;
    }


    /**
     *  Finds the most popular genres for a given customer.
     * @param customerId of the customer.
     * @return the most favorite genre, in case of a tie, returns the tied genres.
     */
    public ArrayList<Genre> getTheMostPopularGenre(int customerId) {
        ArrayList<Genre> genres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            //Query result will be a list of genres the customer has purchased tracks of.
            //with the number of purchases in each track
            // sorted with the most purchased genre first.
            //This query could be transformed into a query that only returns the most popular genres. Performance gain would be negligible.
            prep = conn.prepareStatement(
                    "SELECT genre.Name, genre.GenreId, " +
                            "COUNT(genre.GenreId) as favorites  FROM Genre genre " +
                            "INNER JOIN Track track on genre.GenreId = track.GenreId " +
                            "INNER JOIN InvoiceLine invoiceLine on track.TrackId = invoiceLine.TrackId " +
                            "INNER JOIN Invoice invoice on invoice.InvoiceId = invoiceLine.InvoiceId " +
                            "INNER JOIN Customer customer on customer.CustomerId = invoice.CustomerId " +
                            "WHERE customer.CustomerId = ? GROUP BY genre.GenreId ORDER BY favorites DESC");
            prep.setInt(1, customerId);
            set = prep.executeQuery();
            int favorite = 0;
            //since the ResultSet is sorted we can break the while loop the instant we find something with lower than the favorite score.
            boolean shouldContinue = true;
            while (set.next() && shouldContinue) {
                if( favorite < set.getInt("favorites")) {
                    favorite = set.getInt("favorites");
                }
                //Grabs the genre(s) that is the most popular.
                if (favorite == set.getInt("favorites")) {
                    genres.add( new Genre(
                            set.getInt("GenreId"),
                            set.getString("Name")
                    ));
                }else {
                    shouldContinue = false;
                }

            }

        }catch (SQLException e) {
            System.out.println(e.toString());
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }

        return genres;
    }
}
