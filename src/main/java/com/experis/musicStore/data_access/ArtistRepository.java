package com.experis.musicStore.data_access;

import com.experis.musicStore.models.Artist;

import java.sql.*;
import java.util.ArrayList;

public class ArtistRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn;
    private PreparedStatement prep;
    private ResultSet set;

    /**
     *
     * @return a list of five random artists
     */
    public ArrayList<Artist> getFiveRandomArtists() {
        ArrayList<Artist> artists = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("SELECT ArtistId, Name FROM Artist ORDER BY RANDOM() LIMIT 5");
            set = prep.executeQuery();
            while (set.next()) {
                artists.add( new Artist(
                        set.getInt("ArtistId"),
                        set.getString("Name")
                ));
            }

        }catch (SQLException e) {
            System.out.println(e.toString());
        } finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }

        }
        return artists;
    }

    /**
     *
     * @param name of the artist. Can be partial
     * @return list of artist that have names that matches partially.
     */
    public ArrayList<Artist> getArtistContainingName(String name) {
        name = name.toLowerCase();
        ArrayList<Artist> artists = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            prep = conn.prepareStatement("SELECT ArtistId, Name from Artist WHERE lower (Name) LIKE ?");
            prep.setString(1, "%" + name + "%");
            set = prep.executeQuery();
            while (set.next()) {
                artists.add( new Artist( set.getInt("ArtistId"), set.getString("Name") ));
            }

        }catch (SQLException e) {
            System.out.println(e.toString());
        }finally {
            try {
                set.close();
                prep.close();
                conn.close();
            }catch (SQLException e) {
                System.out.println(e.toString());
            }
        }
        return artists;
    }
}
