FROM openjdk:14
ADD target/musicStore-0.0.1-SNAPSHOT.jar musicStore.jar
ENTRYPOINT [ "java", "-jar", "/musicStore.jar" ]